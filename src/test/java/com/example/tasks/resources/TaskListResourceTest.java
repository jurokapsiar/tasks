package com.example.tasks.resources;

import com.example.tasks.db.DB;
import com.example.tasks.model.TaskList;
import com.example.tasks.model.User;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.SecurityContext;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.*;

public class TaskListResourceTest {

    private static final DB db = mock(DB.class);
    private static final SecurityContext securityContext = mock(SecurityContext.class);

    private final TaskList taskList = new TaskList("testTaskList", new User(5, "testuser"));

    @After
    public void tearDown(){
        reset(db);
    }

    @Test
    public void testGetTaskList() {
        try {
            when(db.getTaskList(eq(1L))).thenReturn(taskList);
            assertThat(new TaskListResource(db).getForId(securityContext, 1L).getName()).isEqualTo("testTaskList");
            verify(db).getTaskList(1L);
        }
        catch (SQLException ex) {
            fail("SQL Exception", ex);
        }
    }
}