package com.example.tasks.serialization;

import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.example.tasks.model.TaskList;
import io.dropwizard.jackson.Jackson;
import org.joda.time.DateTime;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.example.tasks.model.User;

import java.util.Calendar;

public class TaskListTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final TaskList taskList = new TaskList("testTaskList", new User(5, "testuser"));
        DateTime dt = new DateTime(2016, 1, 24, 10, 1, 0, 0);
        taskList.setCreatedAt(dt.toDate());
        taskList.setId(8);

        final String expected = MAPPER.writeValueAsString(MAPPER.readValue(fixture("fixtures/taskList.json"), TaskList.class));

        assertThat(MAPPER.writeValueAsString(taskList)).isEqualTo(expected);

    }

}