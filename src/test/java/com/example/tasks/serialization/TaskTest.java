package com.example.tasks.serialization;

import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.example.tasks.model.Task;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.example.tasks.model.User;

public class TaskTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final Task task = new Task(6, "testcontent");
        task.setDone(true);
        task.setPriority(5);

        final String expected = MAPPER.writeValueAsString(MAPPER.readValue(fixture("fixtures/task.json"), Task.class));

        assertThat(MAPPER.writeValueAsString(task)).isEqualTo(expected);
    }
}