package com.example.tasks.resources;

import com.example.tasks.model.TaskList;
import com.example.tasks.model.User;
import com.example.tasks.db.DB;
import com.example.tasks.model.Task;
import com.j256.ormlite.stmt.PreparedQuery;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/list")
public class TaskListResource {
    private final DB db;

    public TaskListResource(DB db) {
        this.db = db;
    }

    @GET
    @PermitAll
    public List<TaskList> getTaskLists(@Context SecurityContext securityContext) throws SQLException {
        PreparedQuery<TaskList> q = null;
        User user = User.fromContext(securityContext);
        if(User.isAnnonymous(user)) {
            return db.getAnonymousTaskLists();
        }
        else {
            return db.getTaskListsForOwner(user);
        }
    }

    @GET
    @Path("{id}")
    public TaskList getForId(@Context SecurityContext securityContext, @PathParam("id") long id) throws SQLException {
        TaskList tl = db.getTaskList(id);
        if(tl==null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return tl;
    }

    @POST
    @PermitAll
    public TaskList addTaskList(@Context SecurityContext context, @Valid TaskList taskList) throws SQLException {

        taskList.setCreatedAt(new Date());
        if(User.isAnnonymous(User.fromContext(context))) {
            taskList.setOwner(null);
        }
        else {
            taskList.setOwner((User)context.getUserPrincipal());
        }

        db.createTaskList(taskList);
        return taskList;
    }

    @DELETE
    @Path("{id}")
    public Response deleteTaskList(@Context SecurityContext context, @PathParam("id") long id) throws SQLException {
        TaskList tl = getForId(context, id);
        db.deleteTaskList(tl);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}")
    public TaskList modifyTaskList(@Context SecurityContext context, @PathParam("id") long id, @Valid TaskList taskList) throws SQLException {
        if(taskList==null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }

        TaskList old = getForId(context, id);

        old.setName(taskList.getName());
        db.updateTaskList(old);
        return old;
    }


    //tasks
    @GET
    @Path("{id}/task")
    public List<Task> getTasks(@Context SecurityContext securityContext, @PathParam("id") long id) throws SQLException {
        TaskList tl = getForId(securityContext, id);
        return db.getTaskListTasks(tl);
    }

    @GET
    @Path("{id}/task/{taskId}")
    public Task getTaskForId(@Context SecurityContext securityContext, @PathParam("id") long id, @PathParam("id") long taskId) throws SQLException {
        TaskList tl = getForId(securityContext, id);

        if(tl.getId()!=id) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        Task t = db.getTask(taskId);
        if(t==null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return t;
    }

    @POST
    @Path("{id}/task")
    @PermitAll
    public Response addTask(@Context SecurityContext context, @Valid Task task, @PathParam("id") long id) throws SQLException {

        TaskList tl = getForId(context, id);

        Task t = new Task();
        t.setContent(task.getContent());
        t.setPriority(task.getPriority());
        t.setDone(task.isDone());
        t.setTaskList(tl);

        db.createTask(t);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}/task/{taskId}")
    public Response deleteTaskList(@Context SecurityContext securityContext, @PathParam("id") long id, @PathParam("taskId") long taskId) throws SQLException {
        List<Task> tasks = getTasks(securityContext, id);
        for(Task t : tasks) {
            if(t.getId()==taskId) {
                db.deleteTask(t);
                return Response.ok().build();
            }
        }

        throw new WebApplicationException(Response.Status.BAD_REQUEST);
    }

    @PUT
    @PermitAll
    @Path("{id}/task/{taskId}")
    public Task modifyTask(@Context SecurityContext context, @PathParam("id") long id, @PathParam("taskId") long taskId, @Valid Task task) throws SQLException {
        if(task==null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }

        List<Task> tasks = getTasks(context, id);
        for(Task t : tasks) {
            if(t.getId()==taskId) {

                t.setPriority(task.getPriority());
                t.setContent(task.getContent());
                t.setDone(task.isDone());
                db.updateTask(t);

                return t;
            }
        }

        throw new WebApplicationException(Response.Status.BAD_REQUEST);
    }

}
