package com.example.tasks.resources;

import com.example.tasks.model.SigninUser;
import com.example.tasks.model.User;
import com.example.tasks.db.DB;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.NoSuchElementException;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/user")
public class UserResource {
    private final DB db;

    public UserResource(DB db) {
        this.db = db;
    }

    @POST
    public User create(@Valid SigninUser user) throws SQLException {

        user.setName(user.getName().toLowerCase());
        boolean userExists = true;
        try {
           db.getUser(user.getName());
        }
        catch(NoSuchElementException e) {
            userExists = false;
        }

        if(userExists) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        if(!user.getPassword().equals(user.getPassword2())) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        user.setPassword2(null);

        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));

        db.createUser(user);
        return user;
    }

    @POST
    @Path("auth")
    public User authenticate(@Valid User user) throws SQLException {
        //TODO: this should return a token, not a password
        User existing;
        try {
            existing = db.getUser(user.getName());
        }
        catch (NoSuchElementException ex) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }

        if(!BCrypt.checkpw(user.getPassword(), existing.getPassword())) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }

        return existing;
    }
}
