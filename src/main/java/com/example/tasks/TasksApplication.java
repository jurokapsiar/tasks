package com.example.tasks;

import com.example.tasks.auth.TaskAuthorizer;
import com.example.tasks.auth.TaskOAuthAuthenticator;
import com.example.tasks.db.DB;
import com.example.tasks.db.DBHelathcheck;
import com.example.tasks.db.DBImpl;
import com.example.tasks.model.User;
import com.example.tasks.config.TaskConfig;
import com.example.tasks.resources.TaskListResource;
import com.example.tasks.resources.UserResource;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.sql.SQLException;

/**
 * Main class of the application. Registers all resources required for the application to run.
 * @author juro
 */
public class TasksApplication extends Application<TaskConfig> {

    /**
     * Run method is executed by the main entrypoint
     * @param taskConfig the configuration loaded from the config.yml
     * @param environment environment provided by dropwizard
     * @throws SQLException if the db initialization failed
     */
    @Override
    public void run(TaskConfig taskConfig, Environment environment) throws SQLException {

        final DB db = new DBImpl();

        final TaskOAuthAuthenticator taskAuthenticator = new TaskOAuthAuthenticator(db);

        final TaskListResource taskResource = new TaskListResource(db);
        final UserResource userResource = new UserResource(db);

        environment.jersey().register(new AuthDynamicFeature(
                new OAuthCredentialAuthFilter.Builder<User>()
                        .setAuthenticator(taskAuthenticator)
                        .setAuthorizer(new TaskAuthorizer())
                        .setPrefix("Bearer")
                        .buildAuthFilter()));

        environment.jersey().register(taskResource);
        environment.jersey().register(userResource);
        environment.healthChecks().register("dbHealthcheck", new DBHelathcheck(db));

                environment.lifecycle().manage(db);
        environment.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    }

    /**
     * Adds the assets bundle
     * @param bootstrap provided by dropwizard
     */
    @Override
    public void initialize(Bootstrap<TaskConfig> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
    }

    /**
     * Main entrypoint of the application
     * @param args command line parameters
     * @throws Exception if something goes wrong
     */
    public static void main(String[] args) throws Exception {
        new TasksApplication().run(args);
    }
}
