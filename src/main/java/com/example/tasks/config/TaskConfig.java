package com.example.tasks.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

public class TaskConfig extends Configuration {

    @NotEmpty
    @JsonProperty
    private String authenticationCachePolicy;

    public String getAuthenticationCachePolicy() {
        return authenticationCachePolicy;
    }
}
