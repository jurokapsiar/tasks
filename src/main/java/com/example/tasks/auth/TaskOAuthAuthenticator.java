package com.example.tasks.auth;

import com.example.tasks.db.DB;
import com.example.tasks.model.User;
import com.google.common.base.Optional;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

public class TaskOAuthAuthenticator implements Authenticator<String, User> {

    private final DB db;

    public TaskOAuthAuthenticator(DB db) {
        this.db = db;
    }
    
    public Optional<User> authenticate(String s) throws AuthenticationException {
        try {

            if(s.equals(User.ANNONYMOUS.getName())) {
                return Optional.of(User.ANNONYMOUS);
            }

            User user = db.getUserWithToken(s);
            return Optional.of(user);
        } catch (Exception e) {
            return Optional.absent();
        }
    }
}
