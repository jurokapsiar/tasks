package com.example.tasks.auth;

import com.example.tasks.db.DB;
import com.example.tasks.model.User;
import com.google.common.base.Optional;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import org.apache.commons.lang3.StringUtils;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.NoSuchElementException;

public class TaskAuthenticator implements Authenticator<BasicCredentials, User> {

    private final DB db;

    public TaskAuthenticator(DB db) {
        this.db = db;
    }

    @Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {

        if(!StringUtils.isEmpty(credentials.getUsername())) {

            if (credentials.getUsername().equals(User.ANNONYMOUS.getName())) {
                return Optional.of(User.ANNONYMOUS);
            } else {

                try {
                    User user = db.getUser(credentials.getUsername());
                    if (BCrypt.checkpw(credentials.getUsername(), user.getPassword())) {
                        return Optional.of(user);
                    }
                } catch (NoSuchElementException ex) {
                    return Optional.absent();
                }
                 catch (SQLException ex) {
                     return Optional.absent();
                 }
            }
        }

        return Optional.absent();
    }
}