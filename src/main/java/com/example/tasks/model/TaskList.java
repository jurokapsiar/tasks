package com.example.tasks.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

@DatabaseTable(tableName = "tasklists")
public class TaskList {

    @DatabaseField(generatedId = true)
    private long id;

    @Length(max = 200)
    @DatabaseField(canBeNull = false, width = 200)
    @NotBlank
    private String name;

    @DatabaseField(index = true, foreign = true)
    private User owner;

    @DatabaseField(index = true)
    private Date createdAt;

    public TaskList() {

    }

    public TaskList(String name, User owner) {
        this.name = name;
        this.createdAt = new Date();
        this.owner = owner;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @JsonProperty
    public Date getCreatedAt() {
        if(createdAt==null) {
            return null;
        }
        else {
            return new Date(createdAt.getTime());
        }
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt==null) {
            this.createdAt = null;
        }
        else {
            this.createdAt = new Date(createdAt.getTime());
        }
    }
}
