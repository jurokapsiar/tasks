package com.example.tasks.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Optional;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

@DatabaseTable(tableName = "users")
public class User implements Principal {

    public static final User ANNONYMOUS = new User(-1, "Annonymous");

    public static boolean isAnnonymous(Optional<User> u) {
        return u==null || !u.isPresent() || u.get().getName().equals(User.ANNONYMOUS.getName());
    }

    public static boolean isAnnonymous(User u) {
        return u==null || u.getName().equals(User.ANNONYMOUS.getName());
    }

    public static User fromContext(SecurityContext context) {
        if(context==null || context.getUserPrincipal() == null || !(context.getUserPrincipal() instanceof User)) {
            return ANNONYMOUS;
        }
        else {
              return (User) context.getUserPrincipal();
        }
    }


    @DatabaseField(generatedId = true)
    private long id;

    @Length(max = 100)
    @NotBlank
    @DatabaseField(canBeNull = false, width = 100, unique = true, index = true)
    private String name;

    @DatabaseField(canBeNull = false, width = 100)
    @NotBlank
    private String password;

    public User() {

    }

    public User(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    //do not serialize
    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
