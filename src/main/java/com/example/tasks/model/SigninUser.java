package com.example.tasks.model;

import org.hibernate.validator.constraints.NotBlank;

public class SigninUser extends User {

    public SigninUser() {

    }

    @NotBlank
    private String password2;

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}
