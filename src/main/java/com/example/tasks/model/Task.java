package com.example.tasks.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@DatabaseTable(tableName = "tasks")
public class Task {

    @DatabaseField(generatedId = true)
    private long id;

    @Length(max = 200)
    @DatabaseField(canBeNull = false, width = 200)
    @NotBlank
    private String content;

    @DatabaseField(index = true)
    private Integer priority;

    @DatabaseField(index = true, foreign = true)
    private TaskList taskList;

    @DatabaseField
    private boolean done = false;

    public Task() {

    }

    public Task(long id, String content) {
        this.id = id;
        this.content = content;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TaskList getTaskList() {
        return taskList;
    }

    public void setTaskList(TaskList taskList) {
        this.taskList = taskList;
    }

    @JsonProperty
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @JsonProperty
    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
