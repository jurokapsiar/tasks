package com.example.tasks.db;


import com.example.tasks.model.Task;
import com.example.tasks.model.TaskList;
import com.example.tasks.model.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

public class DBImpl implements Managed, DB {

    private static final Logger LOG = LoggerFactory.getLogger(DBImpl.class);

    private ConnectionSource connectionSource;

    private Dao<Task, Long> tasks;
    private Dao<TaskList, Long> taskLists;
    private Dao<User, Long> users;

    public DBImpl() throws SQLException{
    }

    @Override
    public boolean isOpen() {
        return connectionSource!=null && connectionSource.isOpen();
    }

    @Override
    public void start() throws Exception {
        connectionSource =
                new JdbcConnectionSource("jdbc:h2:mem:tasks");

        users = DaoManager.createDao(connectionSource, User.class);
        taskLists = DaoManager.createDao(connectionSource, TaskList.class);
        tasks = DaoManager.createDao(connectionSource, Task.class);


        TableUtils.createTableIfNotExists(connectionSource, User.class);
        TableUtils.createTableIfNotExists(connectionSource, TaskList.class);
        TableUtils.createTableIfNotExists(connectionSource, Task.class);

        tasks.executeRaw("ALTER TABLE TASKS ADD FOREIGN KEY (tasklist_id) REFERENCES TASKLISTS(ID) ON DELETE CASCADE");
        taskLists.executeRaw("ALTER TABLE TASKLISTS ADD FOREIGN KEY (owner_id) REFERENCES USERS(ID) ON DELETE CASCADE");

    }

    @Override
    public void stop() throws Exception {
        connectionSource.close();
        connectionSource = null;
        tasks = null;
        taskLists = null;
        users = null;
    }

    //TODO: need to validate imputs
    //it works fine currently, because the inputs are validated above (@Valid and @Path annotations take care of that mostly)
    //but good practice would be to do propper checking here as well.

    @Override
    public List<TaskList> getAnonymousTaskLists() throws SQLException {
        return taskLists.query(taskLists.queryBuilder().orderBy("createdAt", false).where().isNull("owner_id").prepare());
    }

    @Override
    public List<TaskList> getTaskListsForOwner(User owner) throws SQLException {
        return taskLists.query(taskLists.queryBuilder().orderBy("createdAt", false).where().eq("owner_id", owner.getId()).prepare());
    }

    @Override
    public TaskList getTaskList(Long id) throws SQLException {
        return taskLists.queryForId(id);
    }

    @Override
    public int createTaskList(TaskList taskList) throws SQLException {
        return taskLists.create(taskList);
    }

    @Override
    public int deleteTaskList(TaskList taskList) throws SQLException {
        return taskLists.deleteById(taskList.getId());
    }

    @Override
    public int updateTaskList(TaskList taskList) throws SQLException {
        return taskLists.update(taskList);
    }

    @Override
    public List<Task> getTaskListTasks(TaskList taskList) throws SQLException {
        return tasks.query(tasks.queryBuilder().orderBy("priority", false).where().eq("taskList_id", taskList.getId()).prepare());
    }

    @Override
    public Task getTask(Long id) throws SQLException {
        return tasks.queryForId(id);
    }

    @Override
    public int createTask(Task task) throws SQLException {
        return tasks.create(task);
    }

    @Override
    public int deleteTask(Task task) throws SQLException {
        return tasks.deleteById(task.getId());
    }

    @Override
    public int updateTask(Task task) throws SQLException {
        return tasks.update(task);
    }

    @Override
    public User getUser(String name) throws SQLException, NoSuchElementException {
        List<User> u = users.queryForEq("name", name.toLowerCase());
        if(u.size()>0) {
            return u.get(0);
        }
        else {
            throw new NoSuchElementException("User not found");
        }
    }

    @Override
    public User getUserWithToken(String token) throws SQLException, NoSuchElementException {
        //TODO: use token instead of password
        List<User> u = users.queryForEq("password", token);
        if(u.size()>0) {
            return u.get(0);
        }
        else {
            throw new NoSuchElementException("User not found");
        }
    }


    @Override
    public int createUser(User user) throws SQLException {
        if(user.getName()!=null) {
            user.setName(user.getName().toLowerCase());
        }
        return users.create(user);
    }


}
