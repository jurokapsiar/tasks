package com.example.tasks.db;

import com.codahale.metrics.health.HealthCheck;
import com.example.tasks.db.DB;

public class DBHelathcheck extends HealthCheck {
    private final DB db;

    public DBHelathcheck(DB db) {
        this.db = db;
    }

    @Override
    protected Result check() throws Exception {
        if (db.isOpen()) {
            return Result.healthy();
        }
        return Result.unhealthy("Database connection is closed");
    }
}
