package com.example.tasks.db;

import com.example.tasks.model.Task;
import com.example.tasks.model.TaskList;
import com.example.tasks.model.User;
import io.dropwizard.lifecycle.Managed;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Interface for all db calls
 * @Author juro
 */
public interface DB extends Managed {

    /**
     * Healthcheck call for the db
     * @return true if the connection is alive, false otherwise
     */
    boolean isOpen();

    /**
     * Get task lists owned by annonymous users
     * @return list of task lists
     * @throws SQLException
     */
    List<TaskList> getAnonymousTaskLists() throws SQLException;

    /**
     * Get task lists for an owner. Fails ugly if the owner is null.
     * @param owner the owner
     * @return list of task lists
     * @throws SQLException
     */
    List<TaskList> getTaskListsForOwner(User owner) throws SQLException;

    /**
     * Get task list by id. Fails ugly if the id parameter is null.
     * @param id id of the task list
     * @return single task list
     * @throws SQLException
     */
    TaskList getTaskList(Long id) throws SQLException;

    /**
     * Creates the task list. Fails ugly if the taskList parameter is null. Validation is handled by the model.
     * @param taskList task list to create. id will be assigned by the db layer
     * @return number of affected rows
     * @throws SQLException
     */
    int createTaskList(TaskList taskList) throws SQLException;

    /**
     * Deletes the task list. Fails ugly if the taskList parameter or the taskList.id property is null.
     * @param taskList task list to delete
     * @return number of affected rows
     * @throws SQLException
     */
    int deleteTaskList(TaskList taskList) throws SQLException;

    /**
     * Updates the task list. Fails ugly if the taskList parameter or the taskList.id property is null.
     * @param taskList task list to update (with the new values)
     * @return number of affected rows
     * @throws SQLException
     */
    int updateTaskList(TaskList taskList) throws SQLException;

    /**
     * Gets the list of tasks for a task list.  Fails ugly if the taskList parameter or the taskList.id property is null.
     * @param taskList task list to
     * @return
     * @throws SQLException
     */
    List<Task> getTaskListTasks(TaskList taskList) throws SQLException;

    /**
     * Gets the task for an id. Fails ugly if the id parameter is null
     * @param id id of the task
     * @return the matching task
     * @throws SQLException
     */
    Task getTask(Long id) throws SQLException;

    /**
     * Creates the task. Fails ugly if the task parameter is null. Validation is handled by the model.
     * @param task task to create. id will be assigned by the db layer
     * @return number of affected rows
     * @throws SQLException
     */
    int createTask(Task task) throws SQLException;

    /**
     * Deletes the task. Fails ugly if the task parameter or task.id is null
     * @param task task to create. id will be assigned by the db layer
     * @return number of affected rows
     * @throws SQLException
     */
    int deleteTask(Task task) throws SQLException;

    /**
     * Deletes the task. Fails ugly if the task parameter or task.id is null. Validation handled by the model.
     * @param task task to create. id will be assigned by the db layer
     * @return number of affected rows
     * @throws SQLException
     */
    int updateTask(Task task) throws SQLException;

    /**
     * Gets the user object for a name (login)
     * @param name login name of the user
     * @return the user object
     * @throws SQLException
     * @throws NoSuchElementException if the user is not present
     */
    User getUser(String name) throws SQLException, NoSuchElementException;

    /**
     * Gets the user object for a token (oauth)
     * @param token token of the user
     * @return the user object
     * @throws SQLException
     * @throws NoSuchElementException if the token/user is not present
     */
    User getUserWithToken(String token) throws SQLException, NoSuchElementException;

    /**
     * Creates the user
     * @param user user to create. Name is transformed to lowercase inside of this method. Validation is handled by the model.
     * @return number of affected rows
     * @throws SQLException
     */
    int createUser(User user) throws SQLException;
}
