# README #

This is a simple Task Lists application.

### How do I run it? ###

* after cloning, execute `mvn compile exec:java`
* Open `localhost:8080` in your favorite browser

### How do I test it? ###

* `mvn test`

### TO DO ###

* Finish DB layer refactoring for better testability
* Write more unit tests
* Some more checks in the db layer
* More user friendly messages in the UI
* Logout
* Better and more secure OAuth token handling
* More javadocs

### Who do I talk to? ###

* jurokapsiar (at) gmail.com
